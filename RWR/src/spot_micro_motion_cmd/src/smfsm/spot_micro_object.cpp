#include "spot_micro_object.h"
#include "spot_micro_motion_cmd.h"
#include "spot_micro_transition_stand.h"

SpotMicroObjectDetectedState::SpotMicroObjectDetectedState() {
  // Construcotr, doesn't need to do anything, for now...
  //std::cout << "SpotMicroIdleState Ctor" << std::endl;
}

SpotMicroObjectDetectedState::~SpotMicroObjectDetectedState() {
  //std::cout << "SpotMicroIdleState Dtor" << std::endl;
}

void SpotMicroObjectDetectedState::handleInputCommands(const smk::BodyState& body_state,
                                             const SpotMicroNodeConfig& smnc,
                                             const Command& cmd,
                                             SpotMicroMotionCmd* smmc,
                                             smk::BodyState* body_state_cmd_) {
  if (smnc.debug_mode) {
    std::cout << "In Spot Micro Object Detected State" << std::endl;
  }
  
  // Check if object is detected anymore, if not transition to the stand
  if (true) {
    //changeState(smmc, std::make_unique<SpotMicroTransitionStandState>());
    
  } else {
    // Otherwise, just carry on being in object detection state
    //smmc->publishZeroServoAbsoluteCommand();
  }

}

