#ifndef OBJECT_DET_H
#define OBJECT_DET_H

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

class ObjectDet
{
    public:
        ObjectDet(ros::NodeHandle & nh);
        ~ObjectDet();

    private:
        bool called;
        ros::NodeHandle nh_;
        ros::Publisher walk_cmd;
        ros::Subscriber walk_stat;
        ros::Publisher stand_cmd;
        ros::Subscriber stand_stat;
        ros::Publisher cmd_vel;
        ros::Subscriber obj_det;
        ros::Subscriber dodge_dir;
        //ros::Subscriber curr_state;

        std_msgs::Bool standStat;
        std_msgs::Bool walkStat;

        void ObjectDetectedCallback(const std_msgs::String& commands);
        void standStatCallback(const std_msgs::Bool::ConstPtr& det);
        void walkStatCallback(const std_msgs::Bool::ConstPtr& det);
        void dodgeDirCallback(const std_msgs::String::ConstPtr& dir);
};

#endif //OBJECT_DET_H