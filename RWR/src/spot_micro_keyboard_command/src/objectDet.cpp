#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_datatypes.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
//#include <geometry_msgs/msg/vector3.h>

#include "objectDet.h"

ObjectDet::ObjectDet(ros::NodeHandle & nh) : nh_(nh)
{
    called = false; 

    stand_cmd = nh_.advertise<std_msgs::Bool>("/stand_cmd", 1);
    walk_cmd = nh_.advertise<std_msgs::Bool>("/walk_cmd", 1);
    cmd_vel = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
    bool stand_stat = true;
    obj_det = nh_.subscribe("/obj_det", 1, &ObjectDet::ObjectDetectedCallback, this);
}

ObjectDet::~ObjectDet()
{}

geometry_msgs::Twist moveLeft(void)
{
    geometry_msgs::Twist vel_cmd_msg;
    vel_cmd_msg.linear.x = 0;
    vel_cmd_msg.linear.y = -0.08;
    vel_cmd_msg.linear.z = 0;
    vel_cmd_msg.angular.x = 0;
    vel_cmd_msg.angular.y = 0;
    vel_cmd_msg.angular.z = 0;

    return vel_cmd_msg;
}

geometry_msgs::Twist moveRight(void)
{
    geometry_msgs::Twist vel_cmd_msg;
    vel_cmd_msg.linear.x = 0.0;
    vel_cmd_msg.linear.y = 0.08;
    vel_cmd_msg.linear.z = 0.0;
    vel_cmd_msg.angular.x = 0.0;
    vel_cmd_msg.angular.y = 0.0;
    vel_cmd_msg.angular.z = 0.0;

    return vel_cmd_msg;
}

geometry_msgs::Twist moveReset(void)
{
    geometry_msgs::Twist vel_cmd_msg;
    vel_cmd_msg.linear.x = 0.0;
    vel_cmd_msg.linear.y = 0.0;
    vel_cmd_msg.linear.z = 0.0;
    vel_cmd_msg.angular.x = 0.0;
    vel_cmd_msg.angular.y = 0.0;
    vel_cmd_msg.angular.z = 0.0;

    return vel_cmd_msg;
}

void ObjectDet::standStatCallback(const std_msgs::Bool::ConstPtr& det)
{

    standStat.data = det->data;
    return;
}

void ObjectDet::walkStatCallback(const std_msgs::Bool::ConstPtr& det)
{
    walkStat.data = det->data;
    return;
}

void ObjectDet::ObjectDetectedCallback(const std_msgs::String& det)
{
    walk_stat = nh_.subscribe("/walk_cmd", 1, &ObjectDet::walkStatCallback, this);
    
    if(!walkStat.data)
    {
        stand_stat = nh_.subscribe("/stand_cmd", 1, &ObjectDet::standStatCallback, this);
    }

    std_msgs::Bool stand_msg;
    std_msgs::Bool walk_msg;
    geometry_msgs::Twist moveVel;
    //std_msgs::Vector3 moveAng;

    stand_msg.data = true;
    walk_msg.data = true;
    stand_cmd.publish(stand_msg);
    ros::Duration(3.0).sleep();
    if(!standStat.data)
    {
        //stand_cmd.publish(stand_msg);
        //ros::Duration(2.5).sleep();
    }
    
    if(!walkStat.data)
    {
        walk_cmd.publish(walk_msg);
        ros::Duration(0.5).sleep();
    }

    moveVel = moveReset();
    cmd_vel.publish(moveVel);
    ros::Duration(0.01).sleep();

    if(det.data.compare("right") == 0)
    {
        moveVel = moveRight();
        cmd_vel.publish(moveVel);
    }
    else if (det.data.compare("left") == 0)
    {
        moveVel = moveLeft();
        cmd_vel.publish(moveVel);
    }
    else if (det.data.compare("none") == 0)
    {
        moveVel = moveReset();
        cmd_vel.publish(moveVel);
    }

    standStat.data = false;
    walkStat.data = false;
    ros::Duration(5.0).sleep();

    called = true;
    // ROS_INFO_STREAM("No Object detected or already in walk mode");   
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "move_publisher");

    ros::NodeHandle nh;
    ObjectDet detected(nh);

    ros::spin();
    return 0;
}