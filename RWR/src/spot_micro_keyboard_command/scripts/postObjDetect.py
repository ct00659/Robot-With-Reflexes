#!/usr/bin/python3

from typing import Match
import rospy
import sys, select, termios, tty # For terminal keyboard key press reading
from std_msgs.msg import Bool 
from std_msgs.msg import String 
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist 


from math import pi

called = False

def setRightVel():
    _vel_cmd_msg = Twist()
    _vel_cmd_msg.linear.x = 0
    _vel_cmd_msg.linear.y = 0.4
    _vel_cmd_msg.linear.z = 0
    _vel_cmd_msg.angular.x = 0
    _vel_cmd_msg.angular.y = 0
    _vel_cmd_msg.angular.z = 0
    return move_vel

def setRightAng():    
    move_ang = Vector3()
    move_ang.x = 0.4
    move_ang.y = 0.0
    move_ang.z = 0.0
    return move_ang

def setLeftVel():
    _vel_cmd_msg = Twist()
    _vel_cmd_msg.linear.x = 0
    _vel_cmd_msg.linear.y = -0.4
    _vel_cmd_msg.linear.z = 0
    _vel_cmd_msg.angular.x = 0
    _vel_cmd_msg.angular.y = 0
    _vel_cmd_msg.angular.z = 0
    return move_vel

def setLeftAng():
    move_ang = Vector3()
    move_ang.x = -0.4
    move_ang.y = 0.0
    move_ang.z = 0.0
    return move_ang 

def dodgeDirCallback(dodge_dir):
    moveDirVel = Twist()
    moveDirAng = Vector3()
    #dodgeCmd_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
    #dodgeCmd_ang = rospy.Publisher('/angle_cmd' ,Vector3, queue_size=1)
    moveDirVelPub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
    moveDirAngPub = rospy.Publisher('/angle_cmd' ,Vector3, queue_size=1)
    
    if dodge_dir == "Left":
        rospy.loginfo("Going Left")
        moveDirVel = setLeftVel()
        moveDirAng = setLeftAng()

    if dodge_dir == "Right":
        rospy.loginfo("going right")
        moveDirVel = setRightVel()
        moveDirAng = setRightAng()

    if dodge_dir == "None":
        rospy.loginfo("No object movement needed")
    
    moveDirVelPub.publish(moveDirVel)
    moveDirAngPub.publish(moveDirAng)
    
    return


def objCallback(obj_det):
    #inputArgs = Twist()
    called = False

    if obj_det == True:
        called = True
        stand_msg = Bool()
        stand_msg.data = True
        walk_msg = Bool()
        walk_msg.data = True
        ##_vel_cmd_msg = Twist()
        _vel_cmd_msg = Twist()
        _vel_cmd_msg.linear.x = 0
        _vel_cmd_msg.linear.y = 0.4
        _vel_cmd_msg.linear.z = 0
        _vel_cmd_msg.angular.x = 0
        _vel_cmd_msg.angular.y = 0
        _vel_cmd_msg.angular.z = 0

        #obj_det = rospy.Subscriber('/obj_det', Bool)

        #standPub = rospy.Publisher('/stand_cmd',Bool,queue_size=1)
        #standPub.publish(stand_msg)
        
        walkPub = rospy.Publisher('/walk_cmd',Bool, queue_size=1)
        walkPub.publish(walk_msg)
        
        #moveDir = setRightVel()
        #moveDirAng = setRightAng()
        moveVelPub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        moveAngPub = rospy.Publisher('/angle_cmd', Vector3, queue_size=1)
        
        #moveVelPub.publish(_vel_cmd_msg)
        #rospy.loginfo("issued obj detected - transitioning to walk mode")        

        #rospy.Subscriber('dodge_dir', String, dodgeDirCallback, queue_size=1)

    #else:
    #    if called == True:
    #        rospy.loginfo("issued obj detected already")
    #    else:
    #        rospy.loginfo("No obj called from command line")

    
def obj_det_listener():
    rospy.init_node('obj_det_sub')
    rospy.Subscriber('/obj_det', Bool, objCallback, queue_size=1)
    rospy.spin()

if __name__ == '__main__':
    obj_det_listener()