#ifndef EVENT_ARRAY_SUB_H_
#define EVENT_ARRAY_SUB_H_

#define EVENT_ARRAY_PUBLISHERS 3
#define EVENT_ARRAY_SUBSCRIBERS 1

#include <ros/ros.h>
#include <dvs_msgs/Event.h>
#include <dvs_msgs/EventArray.h>
#include <dvs_msgs/stats.h>

namespace event_array_sub {

class EventArraySub
{
    public:
        EventArraySub(ros::NodeHandle& nh);
        ~EventArraySub();

    private:
        ros::NodeHandle nh_;

        void eventsCallback(const dvs_msgs::EventArray::ConstPtr& msg);

        void publishEventStats();

        ros::Subscriber event_sub_;

        struct EventDetection {
            ros::Publisher events_[2];
            ros::Publisher dirLR_;
            float xy_means_[1];
            size_t xy_variance[1];
            bool traject;
            size_t msgSz;
        };
        size_t votesLeft;
        size_t votesRight;
        size_t trajCounter;

        EventDetection event_detection_stats_;
        uint8_t counter, nCounter;
};

}  //namespace

#endif //EVENT_ARRAY_SUB_H_