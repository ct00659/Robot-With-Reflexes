#include "event_array_sub.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "read_event_pub_node");

    ros::NodeHandle nh;
    event_array_sub::EventArraySub eventArraySub(nh);
    //EventArraySub eventArraySub(nh);
    ros::spin();

    return 0;
}