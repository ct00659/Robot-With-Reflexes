#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/String.h>
#include <cstdint> 


#include "event_array_sub.h"

namespace event_array_sub {

EventArraySub::EventArraySub(ros::NodeHandle& nh) : nh_(nh)
{
    counter = 0;
    nCounter = 0;
    votesLeft = 0;
    votesRight = 0;
    trajCounter = 0;
    event_detection_stats_.events_[0] = nh_.advertise<std_msgs::UInt32>("event_avg_x", 1);
    event_detection_stats_.events_[1] = nh_.advertise<std_msgs::UInt32>("event_avg_y", 1);
    event_detection_stats_.events_[2] = nh_.advertise<std_msgs::UInt32>("event_msg_sz", 1);  
    event_detection_stats_.dirLR_ = nh_.advertise<std_msgs::String>("/obj_det", 1);   

    event_sub_ = nh_.subscribe("/AADC_AudiTT/camera_front/events", 1, &EventArraySub::eventsCallback, this);
}

EventArraySub::~EventArraySub()
{}

void EventArraySub::eventsCallback(const dvs_msgs::EventArray::ConstPtr& msg)
{
    //EventArraySub::EventDetection event_detection_stats_; 
    uint32_t avg_x = 0;
    uint32_t avg_y = 0;
    uint32_t maxPoints = 124;
    uint32_t xYPol = 2;
    uint64_t xVar = 0;
    uint64_t yVar = 0;
    //float sum = 0.0;
    event_detection_stats_.msgSz = msg->events.size();
    uint32_t currFrame[2][event_detection_stats_.msgSz];

    ROS_INFO_STREAM("Number of particles=" << event_detection_stats_.msgSz);

    for (int32_t i(0); i < event_detection_stats_.msgSz; ++i)
    {
        ROS_INFO_STREAM("X=" << msg->events[i].x);
        ROS_INFO_STREAM("Y=" << msg->events[i].y;);
        ROS_INFO_STREAM("pol=" << msg->events[i].polarity);
        
        currFrame[0][i] = msg->events[i].x;
        currFrame[1][i] = msg->events[i].y;
        //currFrame[i][2] = msg->events[i].polarity;
        
        avg_x = avg_x + msg->events[i].x;
        avg_y = avg_y + msg->events[i].y;
    }
    if(msg->events.size() > 0)
    {
        avg_x = floor(avg_x / msg->events.size());
        avg_y = floor(avg_y / msg->events.size());

        //Trajectory - gradient of mean
        /*float trajThresh = 0.01;
        float ds = (avg_y - event_detection_stats_.xy_means_[1]) / (avg_x - event_detection_stats_[0]);
        if(ds > trajThresh)
        {
            ++trajCounter;
            
            if(ds > 0)
            {
                ++votesLeft;
            }
            else
            {
                ++votesRight;
            }
        }
*/
        event_detection_stats_.xy_means_[0] = avg_x;
        event_detection_stats_.xy_means_[1] = avg_y;
        ROS_INFO_STREAM("AVG_X: " << event_detection_stats_.xy_means_[0] << "\t AVG_Y: " << event_detection_stats_.xy_means_[1]);
    
        for (int32_t i(0); i < event_detection_stats_.msgSz; ++i)
        {
          xVar = xVar + ((msg->events[i].x - avg_x) * (msg->events[i].x - avg_x));
          yVar = yVar + ((msg->events[i].y - avg_y) * (msg->events[i].y - avg_y));
        //currFrame[i][0] -= event_detection_stats_.xy_mean_[0];
        //currFrame[1][i] -= event_detection_stats_.xy_mean_[1];
        //sum = sum + (xVar * yVar);
        }
        if(((xVar + yVar)/2) > 30)
        {
          xVar = 60;
          yVar = 60;
        }
        if(((xVar + yVar)/2) < 1)
        {
          xVar = 1;
          yVar = 1;
        }
        xVar = xVar / (msg->events.size());
        yVar = yVar / (msg->events.size());
    }
    else
    {
        event_detection_stats_.xy_means_[0] = 64;
        event_detection_stats_.xy_means_[1] = 64;
    }
    
    event_detection_stats_.xy_variance[0] = xVar;   
    event_detection_stats_.xy_variance[1] = yVar;   
    //ROS_INFO_STREAM("var_X: " << xVar << "\t var_Y: " << yVar);

    publishEventStats(); //not sure why this fails

}

void EventArraySub::publishEventStats()
{
    //calculate mean (u)
    //calculate covariance -- (1/N)(Q)(Q)^T
    // Q = (data set) - (x)
    //Q^T is transposed


    /*std_msgs::Float32 msg;
    msg.data = event_detection_stats_.xy_means_[0];
    event_detection_stats_.events_[0].publish(msg);
    msg.data = event_detection_stats_.xy_means_[1];
    event_detection_stats_.events_[1].publish(msg);
    msg.data = event_detection_stats_.msgSz;
    event_detection_stats_.events_[2].publish(msg);
*/
    std_msgs::String dir;
    float avgVar = (event_detection_stats_.xy_variance[0] + event_detection_stats_.xy_variance[1])/2;
    
    if(nCounter > 10)
    {
        counter = 0;
        nCounter = 0;
    }

    if(avgVar > 1.5)
    {
        ++counter;
        if(counter > 3)
        {
            if(event_detection_stats_.xy_means_[0] < 64)
                dir.data = "right";
            else if (event_detection_stats_.xy_means_[0] > 64)
                dir.data = "left";
            else
                dir.data = "none";
        }
    }
    else
    {
        ++nCounter;
    }
    
    event_detection_stats_.dirLR_.publish(dir);
    ros::Duration(1.0).sleep();
}

} //namespace