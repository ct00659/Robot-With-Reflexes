#include "geometry_msgs/PoseStamped.h"
#include <tf/tf.h>
#include <turtlesim/Pose.h>

#include "wrap_rviz.h"

void pub_rviz_wrapper::turtle_pose_callback(const turtlesim::Pose& msg){
    geometry_msgs::PoseStamped pose_rviz;
    pose_rviz.header.frame_id="map";
    pose_rviz.header.stamp=ros::Time::now();
    pose_rviz.header.seq=0;
    pose_rviz.pose.orientation =
    tf::createQuaternionMsgFromYaw(msg.theta);
    pose_rviz.pose.position.x = msg.x;
    pose_rviz.pose.position.y = msg.y;

    this->pub.publish(pose_rviz);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "pose_subscriber");
    pub_rviz_wrapper wrap;
    ros::NodeHandle nh;
    //ros::Subscriber sub = nh.subscribe("turtle1/pose",10, &pub_rviz_wrapper::turtle_pose_callback);
    ros::Subscriber sub = nh.subscribe("turtle1/pose", 10, &pub_rviz_wrapper::turtle_pose_callback, &wrap);
    //pub = nh.advertise("turtle1/pose_rviz", 10, &pub_rviz_wrapper::turtle_pose_callback, &wrap);
    wrap.pub = nh.advertise<geometry_msgs::PoseStamped>("turtle1/pose_rviz", 10);
    ros::spin();
}