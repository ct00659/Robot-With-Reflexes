#ifndef WRAP_RVIZ_H
#define WRAP_RVIZ_H

#include <ros/ros.h>
#include <turtlesim/Pose.h>

class pub_rviz_wrapper
{
    public:
        ros::Publisher pub;
        void turtle_pose_callback(const turtlesim::Pose& msg);
};

#endif //WRAP_RVIZ_H