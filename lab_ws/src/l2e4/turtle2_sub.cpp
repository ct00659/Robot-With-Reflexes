#include <ros/ros.h>
#include <turtlesim/Pose.h>
#include <turtlesim/Spawn.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_datatypes.h>

void turtle1_command_callback(const turtlesim::Twist &msg)
{
    ROS_INFO_STREAM(msg * -1);
}

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "turtle2_subscriber");
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("turtle2/cmd_vel", 10, turtle1_command_callback);
    ros::spin();

    return 0;
}
