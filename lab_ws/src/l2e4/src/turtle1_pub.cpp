#include <ros/ros.h>
#include <turtlesim/Pose.h>
#include <turtlesim/Spawn.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_datatypes.h>

//#define INVERT(x) x * -1
ros::Publisher turtle2_cmd;

void velocityCallback(const geometry_msgs::Twist& msg)
{
    geometry_msgs::Twist t2_cmd_vel;
    //turtle1_cmd = nh1.subscribe("turtle1/cmd_vel", 1000, &velocityCallback);
    t2_cmd_vel.linear.x  = msg.linear.x * -1;
    t2_cmd_vel.linear.y  = msg.linear.y * -1;
    t2_cmd_vel.angular.x = msg.angular.x * -1;
    t2_cmd_vel.angular.y = msg.angular.y * -1;
    t2_cmd_vel.angular.z = msg.angular.z * -1;

    
    ROS_INFO_STREAM("output:linear.x " << t2_cmd_vel.linear.x << " linear.y " << t2_cmd_vel.linear.y );
    turtle2_cmd.publish(t2_cmd_vel);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "command_pub_sub");
    ros::NodeHandle nh1;
    ros::NodeHandle nh2;
    turtle2_cmd = nh2.advertise<geometry_msgs::Twist>("turtle2/cmd_vel", 1000);
    ros::Subscriber turtle1_cmd = nh1.subscribe("turtle1/cmd_vel", 1000, &velocityCallback);
    //ros::Publisher turtle1_cmd = nh1.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1000);
    
    ros::Rate loop_rate(10);
    //int count(0);

    /*while(ros::ok())
    {
        geometry_msgs::Twist t2_cmd_vel;
        //turtle1_cmd = nh1.subscribe("turtle1/cmd_vel", 1000, &velocityCallback);
        t2_cmd_vel.linear.x  = t2_cmd_vel.linear.x * -1;
        t2_cmd_vel.linear.y  = t2_cmd_vel.linear.y * -1;
        t2_cmd_vel.angular.x = t2_cmd_vel.angular.x * -1;
        t2_cmd_vel.angular.y = t2_cmd_vel.angular.y * -1;
        velocityCallback(t2_cmd_vel, turtle2_cmd);
        //turtle2_cmd.publish(t2_cmd_vel);
        ros::spinOnce();
        loop_rate.sleep();
    }*/

    ros::spin();

    return 0;
}