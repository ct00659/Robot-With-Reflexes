from turtlesim.srv import TeleportAbsolute, Spawn

spawn_client = rospy.ServiceProxy('/spawn', Spawn)
spawn_client(x_pos, y_pos, theta, "turtle3")

teleport_client = rospy.ServiceProxy('/turtle3/teleport_absolute', TeleportAbsolute)
teleport_client(x_pos, y_pos, theta)