#!/usr/bin/env python

import rospy
from turtlesim.msg import Twist

def callback(data):
    print(data)

if __name__ == '__main__':
    rospy.init_node('turtle1_pub_python')
    rospy.Subscriber('turtle1/cmd_vel', Twist, callback, queue_size=1)
    rospy.spin()
    