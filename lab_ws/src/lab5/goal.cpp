#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "gloal_publisher");
    ros::NodeHandle nh;

    ros::Publisher pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1000);
    ros::Rate loop_rate(10);
    int count = 0;

    while(ros::ok())
    {
        geometry_msgs::PoseStamped msg;
        msg.header.frame_id="map";
        msg.header.stamp=ros::Time::now();
        msg.header.seq=count++;

        msg.pose.orientation.x = 0.0;
        msg.pose.orientation.y = 0.0;
        msg.pose.orientation.z = 1.0;

        //msg.pose.orientation = 
        msg.pose.position.x = 1.0;
        msg.pose.position.y = 0.0;
        msg.pose.position.z = 0.0;

        pose_pub.publish(msg);

        ros::spinOnce();
        loop_rate.sleep();
    }
    //working cmd on terminal
    //rostopic pub /move_base_simple/goal geometry_msgs/PoseStamped '{ header: { frame_id:  "map"}, pose: { position: { x: 0.2, y: 0 }, orientation: { x: 0, y: 0, z: 0, w: 1 } } }'

    return 0;
}